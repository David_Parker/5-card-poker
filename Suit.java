public enum Suit {
	//Enums/Suits of the cards
	HEARTS(1) {
		public String toString() {
			return "♥";
		}
	},
	DIAMONDS(2) {
		public String toString() {
			return "♦";
		}
	},
	SPADES(3) {
		public String toString() {
			return "♠";
		}
	},
	CLUBS(4) {
		public String toString() {
			return "♣";
		}
	};
	
	//Fields
	private final int suitNumber;
	
	//Constructor
	private Suit(final int suitNumber) {
		this.suitNumber = suitNumber;
	}
	
	//getMethod
	public int getSuitNumber() {
		return this.suitNumber;
	}
}

	