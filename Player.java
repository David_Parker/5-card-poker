import java.util.InputMismatchException;

public class Player {
	//fields
	private DynamicCardArray hand;
	private int totalChipAmount;
	private int betValue;
	private boolean checked;
	private boolean folded;
	private boolean allIn;
	private boolean eliminated;
	private String betOption;
	private int[] discardedCards;
	private HandRank handRank;
	

	//Constructor
	public Player(int totalChipAmount) {
		this.hand = new DynamicCardArray();
		this.totalChipAmount = totalChipAmount;
		this.betValue = 0;
		this.checked = false;
		this.folded = false;
		this.allIn = false;
		this.eliminated = false;
		this.discardedCards = new int[5];
	}
	
	
	//getMethods

	public DynamicCardArray getHand() {
		return this.hand;
	}
	
	public int getTotalChipAmount() {
		return this.totalChipAmount;
	}

	public int getBetValue() {
		return this.betValue;
	}

	public boolean getChecked() {
		return this.checked;
	}
	
	public boolean getFolded() {
		return this.folded;
	}

	public boolean getAllIn() {
		return this.allIn;
	}

	public boolean getEliminated() {
		return this.eliminated;
	}

	public String getBetOption() {
		return this.betOption;
	}

	public int[] getDiscardedCards() {
		return this.discardedCards;
	}

	public HandRank getHandRank() {
		this.handRank.checkHand();
		return this.handRank;
	}


	//Set methods

	//Sets the bet value
	public void setBetValue(int betValue, int maxBet) {
		if(this.totalChipAmount == betValue) {
			this.betValue = betValue;
		}
		if(betValue < maxBet) {
			throw new InputMismatchException("You cannot bet a value of " +betValue+ ". Bet is less than the current highest bet");
		}
		else if(betValue <= 0) {
            throw new InputMismatchException("You cannot bet a value of " +betValue+ ". Bet is less than 0");
        }
		else if(betValue > this.totalChipAmount) {
			throw new InputMismatchException("You cannot bet a value of " +betValue+ ". Bet is more than your total chips");
		}
		this.betValue = betValue;
	}

	//Sets the bet option to figure out what bet was placed
	public void setBetOption(String betOption, String prevBetOption) {
		//If betOption is not check, bet, or fold while the prevOption was empty or check, then an error is thrown
		if((!betOption.equalsIgnoreCase("check") && !betOption.equalsIgnoreCase("bet") && !betOption.equalsIgnoreCase("fold")) && (prevBetOption.equalsIgnoreCase("") || prevBetOption.equalsIgnoreCase("check"))) {
			throw new IllegalArgumentException(betOption+ " is not a valid option while check is possible!");
        }
		//If betOption is not call, bet, or fold while the prevOption was call or bet, then an error is thrown
        else if((!betOption.equalsIgnoreCase("call") && !betOption.equalsIgnoreCase("bet") && !betOption.equalsIgnoreCase("fold")) && (prevBetOption.equalsIgnoreCase("call") || prevBetOption.equalsIgnoreCase("bet"))) {
			throw new IllegalArgumentException(betOption+ " is not a valid option while check is not possible!");
        }
		this.betOption = betOption;
	}

	//Sets the discarded cards
	public void setDiscardedCards(int discardedCard) {
		if(!(discardedCard >= 0 && discardedCard <= 4)) {
			throw new IllegalArgumentException(discardedCard + " is not valid. Must be between 1 and 5!");
		}
		else if(this.discardedCards[discardedCard] == 1) {
			throw new IllegalArgumentException(discardedCard + " is not valid. Card already discarded!");
		}
		this.discardedCards[discardedCard] = 1;
	}

	//Gives more chips to players
	public void setTotalChipAmount(int chipAmount) {
		this.totalChipAmount += chipAmount;
	}

	//Sets hand and it's hand rank
	public void setHandRank(DynamicCardArray hand) {
		this.handRank = new HandRank(hand);
	}
	//Custom Methods

	//Betting method
	public void bet() {
		this.totalChipAmount -= this.betValue;
		//If they go all in, they are checked to allow other players to check second phase
		if(this.totalChipAmount == 0) {
			placeAllIn();
			check();
		}
	}

	//Checking method
	public void check() {
		this.checked = true;
	}

	//Folding method
	public void fold(DynamicCardArray discardPile) {
		//Discards 5 cards to discard pile
		for(int i=0; i<5; i++) {
			discardPile.addCard(this.hand.getCard(i));
		}
		for(int i=0; i<5; i++) {
			this.hand.removeCard(0);
		}
		this.folded = true;
	}

	//places player all in status
	public void placeAllIn() {
		this.allIn = true;
	}

	//places player in eliminated status
	public void placeEliminated() {
		this.eliminated = true;
	}

	//Calling method
	public int call(int maxBet) {
		int prevBet = this.betValue;
		this.totalChipAmount -= (maxBet-this.betValue);
		this.betValue = maxBet;
		return prevBet;
	}
	

	//Reset methods
	public void checkReset() {
		this.checked = false;
	}
	public void foldReset() {
		this.folded = false;
	}
	public void betValueReset() {
		this.betValue = 0;
	}
	public void betOptionReset() {
		this.betOption = "";
	}

	//Places hand card to discard pile, then copies the first card of the deck to the old hand card position to replace it, and then finally removing the first card from the deck
	public void discardChosenCards(DynamicCardArray discardPile, DynamicCardArray deck) {
		for(int i=0; i<5; i++) {
			if(this.discardedCards[i] == 1) {
				discardPile.addCard(this.hand.getCard(i));
				this.hand.replaceCard(deck.getCard(0), i);
				deck.removeCard(0);
			}
		}
	}

	//Resets discarded cards array to 0
	public void resetDiscardedCards() {
		for(int i=0; i<this.discardedCards.length; i++) {
			this.discardedCards[i] = 0;
		}
	}

	//toString
	public String toString() {
		return this.hand + "\nChip amount: " + this.totalChipAmount;
	}
}