public class HandRank {
    //Fields
    private String name;
    private int value;
    private long specificHandValue;
    private DynamicCardArray hand;

    //Constructor
    public HandRank(DynamicCardArray hand) {
        if(hand.length() != 5) {
            throw new IllegalArgumentException(hand + " is invalid. Must be of length 5!");
        }
        this.name = "";
        this.value = 0;
        this.specificHandValue = 0;
        this.hand = hand;
    }

    //Get methods
    public String getName() {
        return this.name;
    }

    public int getValue() {
        return this.value;
    }

    public long getSpecificHandValue() {
        return this.specificHandValue;
    }

    //Helper methods

    //Returns the current card using the index
    private Card findCard(int index) {
        return this.hand.getCard(index);
    }

    //Returns a hand value for high card and flush
    private void calculateHighAndFlush() {
        this.specificHandValue = 0;
        for(int i=4; i>=0; i--) {
            if(i == 4) {
                this.specificHandValue += findCard(i).getNumberValue() * 100000000;
            }
            else if(i == 3) {
                this.specificHandValue += findCard(i).getNumberValue() * 1000000;
            }
            else if(i == 2) {
                this.specificHandValue += findCard(i).getNumberValue() * 10000;
            }
            else if(i == 1) {
                this.specificHandValue += findCard(i).getNumberValue() * 100;
            }
            else if(i == 0) {
                this.specificHandValue += findCard(i).getNumberValue();
            }
        }
    }

    //Checks if the hand has a pair somewhere
    private boolean hasPair() {
        this.specificHandValue = 0;
        boolean foundPair = false;
        for(int i=1; i<this.hand.length(); i++) {
            //Checks if current card is equal to previous
            if(findCard(i).getNumberValue() == findCard(i-1).getNumberValue()) {
                //Sets hand value
                this.specificHandValue += findCard(i).getNumberValue() * 100;
                foundPair = true;
            }
            else {
                this.specificHandValue += findCard(i-1).getNumberValue();
            }
        }
        return foundPair;
    }

    //Checks if the hand has 2 pairs
    private boolean hasTwoPair() {
        int foundPairs = 0;
        int highestPair = 0;
        int lowestPair = 0;
        this.specificHandValue = 0;
        for(int i=1; i<this.hand.length(); i++) {
            //Checks if current card is equal to previous and adds a counter
            if(findCard(i).getNumberValue() == findCard(i-1).getNumberValue()) {
                //Sets highest pair value to field
                if(findCard(i).getNumberValue() > highestPair) {
                    highestPair = findCard(i).getNumberValue();
                }
                else {
                    lowestPair = findCard(i).getNumberValue();
                }
                foundPairs++;
                i++;
            }
            this.specificHandValue += findCard(i-1).getNumberValue();
        }
        this.specificHandValue += highestPair * 10000;
        this.specificHandValue += lowestPair * 100;
        return foundPairs == 2;
    }

    //Checks if the hand has 3 of the same number card
    private boolean hasThreeOfAKind() {
        int sameNumCount = 1;
        boolean foundThreeOfAKind = false;
        this.specificHandValue = 0;
        for(int i=1; i<this.hand.length(); i++) {
            //Checks if current card is equal to previous and sets counter
            if(findCard(i).getNumberValue() == findCard(i-1).getNumberValue()) {
                sameNumCount++;
                //If counter reaches 3, then there is a 3 of a kind
                if (sameNumCount == 3) {
                    this.specificHandValue += findCard(i).getNumberValue() * 100;
                    foundThreeOfAKind = true;
                }
            }
            else {
                this.specificHandValue += findCard(i-1).getNumberValue();
                sameNumCount = 1;
            }
        }
        return foundThreeOfAKind;
    }

    //Checks if all cards are in a sequence
    private boolean hasStraight() {
        //Checks to see if current card is 1 higher than previous
        this.specificHandValue = 0;
        for(int i=1; i<this.hand.length(); i++) {
            //Check to see if the previous card is not equal to itself + 1
            if(findCard(i).getNumberValue() != findCard(i-1).getNumberValue() + 1) {
                return false;
            }
        }
        this.specificHandValue += findCard(4).getNumberValue();
        return true;
    }

    //Checks if all the cards have the same suit
    private boolean hasFlush() {
        this.specificHandValue = 0;
        //Checks through all cards to see if they all have the same suit
        for(int i=1; i<this.hand.length(); i++) {
            if(findCard(i).getSuitValue() != findCard(i-1).getSuitValue()) {
                return false;
            } 
        }
        return true;
    }

    //Checks if the hand has a full house
    private boolean hasFullHouse() {
        boolean pair = false;
        boolean threeOfAKind = false;
        this.specificHandValue = 0;
        for (int i=0; i < this.hand.length()-2; i++) {
            //Checks if current card is equal to next card
            if(findCard(i).getNumberValue() == findCard(i+1).getNumberValue()) {
                //Checks if the current card is three of a kind if threeOfAKind boolean is false
                if(!threeOfAKind && (findCard(i).getNumberValue() == findCard(i+2).getNumberValue())) {
                    this.specificHandValue += findCard(i).getNumberValue() * 100;
                    //Skips to next possible card after 3 of a kind
                    i += 2;
                    threeOfAKind = true;
                }
                //Has to be a pair if not a three of a kind
                else {
                    this.specificHandValue += findCard(i).getNumberValue();
                    //Skips to next possible card after pair
                    i++;
                    pair = true;
                }
            }
            //Ends loop if after first check there is no three of a kind or pair
            if(i==0 && (!pair && !threeOfAKind)) {
                break;
            }
        }
        return (pair && threeOfAKind);
    }
    
    //Checks if the hand has 4 of the same number card
    private boolean hasFourOfAKind() {
        boolean foundFourOfAKind = false;
        int sameNumCount = 1;
        this.specificHandValue = 0;
        for(int i=1; i<this.hand.length(); i++) {
            //Checks to see if the previous card is the same as current card
            if(findCard(i).getNumberValue() == findCard(i-1).getNumberValue()) {
                sameNumCount++;
                //If count is 4, the four of a kind is found
                if (sameNumCount == 4) {
                    this.specificHandValue += findCard(i).getNumberValue() * 100;
                    foundFourOfAKind = true;
                }
            }
            else {
                this.specificHandValue += findCard(i-1).getNumberValue();
                sameNumCount = 1;
            }
        }
        return foundFourOfAKind;
    }

    //Checks if the hand has a straight flush
    private boolean hasStraightFlush() {
        return (hasFlush() && hasStraight());
    }

    //Custom methods

    //Sets all fields and checks what hand the player has
    public void checkHand() {
        if(hasStraightFlush()) {
            this.name = "Straight Flush";
            this.value = 9;    
        }
        else if(hasFourOfAKind()) {
            this.name = "Four of a kind";
            this.value = 8;    
        }
        else if(hasFullHouse()) {
            this.name = "Full House";
            this.value = 7;    
        }
        else if(hasFlush()) {
            this.name = "Flush";
            this.value = 6;
            calculateHighAndFlush(); 
        }
        else if(hasStraight()) {
            this.name = "Straight";
            this.value = 5;    
        }
        else if(hasThreeOfAKind()) {
            this.name = "Three of a kind";
            this.value = 4;    
        }
        else if(hasTwoPair()) {
            this.name = "Two pair";
            this.value = 3;    
        }
        else if(hasPair()) {
            this.name = "Pair";
            this.value = 2;    
        }
        else {
            this.name = "High card";
            this.value = 1;
            calculateHighAndFlush();
        }
    }

    //toString methods
    public String toString() {
        return "" + this.name;
    }
}