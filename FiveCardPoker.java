import java.util.InputMismatchException;
import java.util.Scanner;
public class FiveCardPoker {
	//Validates betInput
	public static void betInput(Game game, Player player) {
		Scanner reader = new Scanner(System.in);
		int betValue = 0;
		boolean betSuccessful = false;
		//This loop calls placeBet, placing a bet if the input is valid
		while(!betSuccessful) {
			try {
				System.out.println("Place a bet Player: ");
				betValue = reader.nextInt();
				player.setBetValue(betValue, game.getMaxBet());
				betSuccessful = true;
			}
			catch(InputMismatchException e) {
				System.out.println(betValue + " is not valid!");
				reader.nextLine();
			}
		}
	}

	//Validates betType
	public static void betPhaseOption(Player player, Game game) {
		Scanner reader = new Scanner(System.in);
		String betOption = "";
		boolean optionSuccessful = false;
		//Loops while the player hasn't inputted correctly
		while(!optionSuccessful) {
			try {
				//If the previous bet was empty (meaning the first player to bet) or if the previous person checked
				if(game.getPrevBetOption().equals("") || game.getPrevBetOption().equalsIgnoreCase("check")) {
					System.out.println("Choose an option: Check    Bet    Fold");
				}
				else {
					System.out.println("Choose an option Player: Call    Bet    Fold");
				}
				betOption = reader.nextLine();
				player.setBetOption(betOption, game.getPrevBetOption());
				optionSuccessful = true;
			}
			catch (IllegalArgumentException e) {
				System.out.println(betOption + " is not valid. It must be an option! ");
			}
		}
	}

	//Asks the user which card they want to discard
	public static void discardInput(Player player) {
		Scanner reader = new Scanner(System.in);
		boolean discardSuccessful = false;
		int discardIndex = 0;
		System.out.println("Choose which cards to discard (1-5). Type done if finished discarding.");
		System.out.println(player.getHand());
		while(!discardSuccessful) {
			//Loops until all 5 cards are inputted or player types done
			for(int i=0; i<5; i++) {
				String doneInput = reader.nextLine();
				if(doneInput.equalsIgnoreCase("done")) {
					discardSuccessful = true;
					break;
				}
				try {
					discardIndex = Integer.parseInt(doneInput);
					player.setDiscardedCards(discardIndex-1);
				}
				catch (IllegalArgumentException e) {
					System.out.println("Input is not valid. Please enter a valid number or 'done'!");
					i--;
				}
			}
			//If players inputted all 5 cards loop ends
			discardSuccessful = true;
		}
	}

	//Asks the user if they want to play again
	private static boolean playAgain() {
		Scanner reader = new Scanner(System.in);
		System.out.println("Would you like to continue playing?");
		String continuePlaying = reader.nextLine();
		while(!(continuePlaying.equalsIgnoreCase("yes") || continuePlaying.equalsIgnoreCase("no"))) {
			System.out.println("Invalid input. Must be yes or no!");
			System.out.println("Would you like to continue playing?");
			continuePlaying = reader.nextLine();
		}
		return continuePlaying.equalsIgnoreCase("no");
	}

	//Asks the player for the amount of players and creates a game
	private static int playerAmount() {
		Scanner reader = new Scanner(System.in);
		int numPlayers = 0;
		boolean isSuccessful = false;
		//Loops until user enters a number between 2-5
		while(!isSuccessful) { 
			try{
				System.out.println("Enter the amount of players");
				numPlayers = reader.nextInt();
				if(numPlayers >= 2 && numPlayers <= 5) {
					isSuccessful = true;
				}
				System.out.println("Players must be between 2-5!");
			}
			catch (InputMismatchException e) {
				System.out.println("Input must be an integer!");
				reader.next();
			}
		}
		return numPlayers;
	}
	public static void main(String[] args) {
		boolean stopPlaying = false;
		//Creates a new game object, deck, and players
		Game game = new Game(playerAmount());
		game.makeDeck();
		game.createPlayers();
		//Loops until the player says they don't want to play anymore
		while(!stopPlaying) {
			game.gameLoop();
			stopPlaying = playAgain();
		}
	}
}