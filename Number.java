public enum Number {
	//Enums/Number of the cards
	TWO(2) {
		public String toString() {
			return "2";
		}
	},
	THREE(3) {
		public String toString() {
			return "3";
		}
	},
	FOUR(4) {
		public String toString() {
			return "4";
		}
	},
	FIVE(5) {
		public String toString() {
			return "5";
		}
	},
	SIX(6) {
		public String toString() {
			return "6";
		}
	},
	SEVEN(7) {
		public String toString() {
			return "7";
		}
	},
	EIGHT(8) {
		public String toString() {
			return "8";
		}
	},
	NINE(9) {
		public String toString() {
			return "9";
		}
	},
	TEN(10) {
		public String toString() {
			return "10";
		}
	},
	JACK(11) {
		public String toString() {
			return "J";
		}
	},
	QUEEN(12) {
		public String toString() {
			return "Q";
		}
	},
	KING(13) {
		public String toString() {
			return "K";
		}
	},
	ACE(14) {
		public String toString() {
			return "A";
		}
	};
	
	//Fields
	private final int number;
	
	//Constructor
	private Number(final int numbers) {
		this.number = numbers;
	}
	
	//getMethods
	public int getNumber() {
		return this.number;
	}
}