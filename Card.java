public class Card {
	//Fields
	private Suit suit;
	private Number number;
	
	//Constructor
	public Card(Suit suit, Number number) {
		this.suit = suit;
		this.number = number;
	}
	
	//getMethods
	
	//Returns the suit of the card
	public Suit getSuit() {
		return this.suit;
	}
	
	//Returns the card number
	public Number getNumber() {
		return this.number;
	}

	//Return the number value of the card
	public int getNumberValue() {
		return this.number.getNumber();
	}

	//Returns the suit value of the card
	public int getSuitValue() {
		return this.suit.getSuitNumber();
	}


	
	//toString method
	public String toString() {
		if(this.number == Number.TEN) {
			return "-------------\n" +
				"|" + this.number + "         |\n" +
				"|" + this.suit   + "          |\n" +
				"|           |\n" +
				"|           |\n" +
				"|           |\n" +
				"|           |\n" +
				"|           |\n" +
				"|          " +this.suit+"|\n" +
				"|         " +this.number+"|\n" +
				"-------------\n";
		}
		else {
		return 	"-------------\n" +
				"|" + this.number + "          |\n" +
				"|" + this.suit   + "          |\n" +
				"|           |\n" +
				"|           |\n" +
				"|           |\n" +
				"|           |\n" +
				"|           |\n" +
				"|          " +this.suit+"|\n" +
				"|          " +this.number+"|\n" +
				"-------------\n";
		}
	}
}