import java.util.Random;
public class DynamicCardArray {
    private Card[] cards;
    private int amount;
	
	//Constructor
	public DynamicCardArray() {
		this.cards = new Card[100];
		this.amount = 0;
	}

	//getMethods
	
	//Returns card at array position
	public Card getCard(int i) {
		if(i >= this.amount) {
			throw new ArrayIndexOutOfBoundsException("Invalid index " + i);
		}
		return this.cards[i];
	}

	//Returns length of array
	public int length() {
		return this.amount;
	}

	//Custom methods

	//Adds a card to the array of cards at the last position
	public void addCard(Card card) {
		this.cards[this.amount] = card;
		this.amount++;
	}
	
	//Replaces a card at the array position indicated
	public void replaceCard(Card card, int i) {
		if(i >= this.amount) {
			throw new ArrayIndexOutOfBoundsException("Invalid index " + i);
		}
		this.cards[i] = card;
	}
	
	//Removes a card at the array position indicated
	public void removeCard(int i) {
		if(i >= this.amount) {
			throw new ArrayIndexOutOfBoundsException("Invalid index " + i);
		}
		for(int j=i; j < this.amount; j++) {
			this.cards[j] = this.cards[j+1];
		}
		this.amount--;
	}
	
	//Switches cards from position i to random position until the last card
	public void shuffle() {
		Random rand = new Random();
		int shufflePosition = 0;
		for(int i=0; i<this.amount;i++) {
			shufflePosition = rand.nextInt(this.amount);
			Card cardHolder = this.cards[shufflePosition];
			this.cards[shufflePosition] = this.cards[i];
			this.cards[i] = cardHolder;
		}
	}

	//Sorts the cards
	public void sort() {
		int lowestValue = 0;
		for(int i=0; i<this.amount; i++) {
			lowestValue = findMin(i);
			swapCards(lowestValue, i);
		}
	}

	//Finds the lowest value card in the array
	private int findMin(int start) {
		int lowest = start;
		for (int i=start; i < this.amount; i++) {
			//Uses the Number enum value of the card to determine which is lowest
			if (this.cards[i].getNumberValue() < this.cards[lowest].getNumberValue()) {
				lowest = i;
			}
		}
		return lowest;
	}

	//Swaps cards around
	private void swapCards(int position1, int position2) {
		Card tempCard = this.cards[position1];
		this.cards[position1] = this.cards[position2];
		this.cards[position2] = tempCard;
	}

	//toString
	public String toString() {
		String cards = "";
		for(int i=0; i<this.amount; i++) {
			cards += this.cards[i];
		}
		return cards;
	}
}
