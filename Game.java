public class Game {
    //Fields
    private DynamicCardArray deck;
    private DynamicCardArray discardPile;
    private Player[] players;
    private int potValue;
    private int maxBet;
    private String prevBetOption;


    //Constructor
    public Game(int playerAmount) {
        this.deck = new DynamicCardArray();
        this.discardPile = new DynamicCardArray();
        this.players = new Player[playerAmount];
        this.potValue = 0;
        this.maxBet = 0;
        this.prevBetOption = "";
    }


    //Get methods
    public DynamicCardArray getDeck() {
        return this.deck;
    }

    public DynamicCardArray getDiscardPile() {
        return this.discardPile;
    }

    public Player[] getPlayers() {
        return this.players;
    }

    public int getPotValue() {
        return this.potValue;
    }

    public int getMaxBet() {
        return this.maxBet;
    }

    public String getPrevBetOption() {
        return this.prevBetOption;
    }


    //Helper methods

    private void placeAnte() {
        for(int i=0; i<players.length; i++) {
            //Does not place ante for players who are eliminated or have less than 50 chips
            if(this.players[i].getEliminated() || this.players[i].getTotalChipAmount() < 50) {
                continue;
            }
            this.players[i].setTotalChipAmount(-50);
            this.potValue += 50;
        }
    }

    //Places bet into the pot for the player
    private void placeBet(Player player) {
        FiveCardPoker.betInput(this, player); //Inputs bet value
        player.bet(); //Removes chips from the player
        this.potValue += player.getBetValue(); //Increases pot value
    }

    //Puts check status on the player 
    private void placeCheck(Player player) {
        player.check();
    }

    //Puts folded status on the player
    private void placeFold(Player player) {
        player.fold(this.discardPile);
    }

    private void placeCall(Player player) {
        this.potValue += (this.maxBet - player.call(this.maxBet));
    }

    //Finds the highest bet on the table
    private void findHighestBet() {
        for(Player player : this.players) {
            if(player.getBetValue() > this.maxBet) {
                this.maxBet = player.getBetValue();
            }
        }
    }
    
    //Checks if all the players are in check
    private boolean allPlayersChecked() {
        int playersPlaying = this.players.length;
        //Checks to see who is still playing and if they are all checked
        for(int i=0; i<this.players.length; i++) {
            if(this.players[i].getFolded() || this.players[i].getChecked()) {
                playersPlaying--;
            }
        }
        //Returns true if all remaining players are checked
        if(playersPlaying == 0) {
            return true;
        }
        return false;
    }

    //Checks if all the players have the same bet value
    private boolean allSameBet() {
        int playersPlaying = this.players.length;
        int sum = 0;
        //Calculates the sum of all bets
        for(Player player : this.players) {
            sum += player.getBetValue();
        }
        //If sum is 0, then either everyone is checked or it is the first turn
        if(sum != 0) {
            //Checks to see who is still playing and if they all have the same bet value
            for(Player player : this.players) {
                if(player.getFolded() || (player.getBetValue() == this.maxBet)) {
                    playersPlaying--;
                }
            }
            //Returns true if all the remaining players have the same bet
            return playersPlaying == 0;
        }
        return false;
    }

    //Checks if everyone is folded
    private boolean allFolded() {
        int playersPlaying = this.players.length;
        //Checks to see if all the players have folded or all in except for one
        for(Player player : this.players) {
            if(player.getFolded()) {
                playersPlaying--;
            }
        }
        //Returns true if there is only 1 player left not folded
        return playersPlaying == 1;
    }

    //Places a bet option for the player
    private void successfulBet(Player player) {
        if(player.getBetOption().equalsIgnoreCase("check") && (this.prevBetOption.equalsIgnoreCase("check") || this.prevBetOption.equalsIgnoreCase(""))) {
            placeCheck(player);
        }
        else if(player.getBetOption().equalsIgnoreCase("bet")) {
            placeBet(player);
        }
        else if(player.getBetOption().equalsIgnoreCase("call") && this.maxBet != 0) {
            placeCall(player);
        }
        else if(player.getBetOption().equalsIgnoreCase("fold")) {
            placeFold(player);
        }
    }

    //Resets the betting phase for starting the phase
    private void betPhaseReset() {
        this.maxBet = 0;
        this.prevBetOption = "";
        for(int i=0; i<this.players.length; i++) {
            //If player is all in, do not reset his check status
            if(!this.players[i].getAllIn()) {
                this.players[i].checkReset();
                this.players[i].resetDiscardedCards();
            }
            this.players[i].betValueReset();
            this.players[i].betOptionReset();
        }
    }

    //Sets a winner
    private void setWinner(int[] winnersArray, int winner) {
        for(int i=0; i<winnersArray.length; i++) {
            winnersArray[i] = 0;
        }
        winnersArray[winner] = 1;
    }

    //Finds the winners
    private int findWinner(int[] winnersArray) {
        int numWinners = 0;
        for(int i=0; i<winnersArray.length; i++) {
            if(winnersArray[i] == 1) {
                numWinners++;
                System.out.println("Player " +(i+1));
            }
        }
        return numWinners;
    }

    //Sends all remaining cards to the discard pile
    private void discardRemainingCards() {
        for(int i=0; i<this.players.length; i++) {
            //Checks to see if player has folded
            if(this.players[i].getFolded()) {
                continue;
            }
            //Folds the remaining players
            this.players[i].fold(this.discardPile);
        }
    }

    //Returns all the cards from discard pile to deck
    private void returnCardsToDeck() {
        for(int i=0; i<this.discardPile.length(); i++) {
            this.deck.addCard(this.discardPile.getCard(i));
            this.discardPile.removeCard(i);
        }
    }

    //Gives the pot value to the winners and divides it if there is a tie
    private void givePotToWinners(int[] winners, int numWinners) {
        for(int i=0; i<winners.length; i++) {
            if(winners[i] == 1) {
                this.players[i].setTotalChipAmount(this.potValue/numWinners);
            }
        }
        this.potValue = 0;
    }

    //Resets all the folds for the players who folded
    private void resetFolds() {
        for(int i=0; i<this.players.length; i++) {
            this.players[i].foldReset();
        }
    }
    //Custom methods
   
    //Creates a deck of 52 cards and shuffles them
    public void makeDeck() {
		for(Suit suits : Suit.values()) { //Loops according to the suit of the cards
			for(Number numbers : Number.values()) { //Loops according to the numbers of the cards
				Card card = new Card(suits, numbers);
				this.deck.addCard(card);
			}
		}
		this.deck.shuffle();
    }

    //Creates the players
    public void createPlayers() {
        for(int i=0; i < this.players.length; i++) {
            this.players[i] = new Player(2000);
        }
   }

    //Deals the cards to the players
    public void dealCards() {
        //Add cards to each hand 1 at a time
        for(int i=0; i < 5; i++) {
			for(int j=0; j < this.players.length; j++) {
				//Does not deal cards to eliminated players
                if(this.players[j].getEliminated()) {
                    continue;
                }
                this.players[j].getHand().addCard(this.deck.getCard(0));
				this.deck.removeCard(0);
			}
		}
    }

    //Allows players to discard cards
    private void discardPhase() {
        for(int i=0; i<this.players.length; i++) {
            //Skips if the player is folded or eliminated
            if(this.players[i].getFolded() || this.players[i].getEliminated()) {
                continue;
            }
            System.out.print("\033[H\033[2J"); //Clears screen
		    System.out.flush(); //Resets cursor position 
            System.out.println("Player " +(i+1)+ ":");
            FiveCardPoker.discardInput(this.players[i]);
            this.players[i].discardChosenCards(this.discardPile, this.deck);
        }
    }

    //Simulates betting phase of the game
    private boolean bettingPhase() {
        boolean turnOver = false;
        betPhaseReset(); //Resets player and game values to start a new betting phase
        while(!turnOver) {
            //Loops through every player, skips player if folded
            for(int i=0; i<this.players.length; i++) {
                findHighestBet(); //Find the highest bet
                //See if the loop ends
                if(allFolded() || allPlayersChecked() || allSameBet()) {
                    //Break out the bettingPhase loop
                    turnOver = true;
                    break;
                }
                //If the player is folded, eliminated, or is all in, skip turn
                System.out.println(this.players[i].getChecked());
                if(this.players[i].getFolded() || this.players[i].getEliminated() || this.players[i].getAllIn()) {
                    continue;
                }
                //Sorts all the players' cards when it is their turn
                this.players[i].getHand().sort();
                System.out.print("\033[H\033[2J"); //Clears screen
		        System.out.flush(); //Resets cursor position 
                System.out.println("Player " +(i+1)+ ":");
                System.out.println(this.players[i]);
                FiveCardPoker.betPhaseOption(this.players[i], this); //Inputting bet
                successfulBet(this.players[i]); //Places bet
                //If person folded then prevOption is not filled
                if(!this.players[i].getBetOption().equalsIgnoreCase("fold")) {
                    this.prevBetOption = this.players[i].getBetOption();
                }
            }
        }
        //Returns true if the reason the loop ended was because everyone folded
        return allFolded();
    }

    //Compares all the hands and determines a winner
    private int[] comparisonPhase() {
        int[] winners = new int[this.players.length];
        int highestValue = 0;
        long highestSpecificHandValue = 0;
        for(int i=0; i<this.players.length; i++) {
            //Checks to see if player has folded or eliminated
            if(this.players[i].getFolded() || this.players[i].getEliminated()) {
                continue;
            }
            //Sets the player's hand rank
            this.players[i].setHandRank(this.players[i].getHand());
            //Compares the current player's hand rank to the highest hand rank
            if(this.players[i].getHandRank().getValue() > highestValue) {
                setWinner(winners, i);
                highestValue = this.players[i].getHandRank().getValue();
            }
            //Only goes in this loop if both players have the same hand
            else if(this.players[i].getHandRank().getValue() == highestValue) {
                //Compares the current player's hand value to the highest hand value
                if(this.players[i].getHandRank().getSpecificHandValue() > highestSpecificHandValue) {
                    setWinner(winners, i);
                    highestSpecificHandValue = this.players[i].getHandRank().getSpecificHandValue();
                }
                else if(this.players[i].getHandRank().getSpecificHandValue() == highestSpecificHandValue) {
                    //Sets another winner
                    winners[i] = 1;
                }
            }
        }
        return winners;
    }

    //Plays the game
    public void gameLoop() {
        boolean isGameOver = false;
        int[] winners = new int[this.players.length];
        placeAnte();
        while(!isGameOver) {
            //Deals the cards to the players
            dealCards();
            //First betting phase
            isGameOver = bettingPhase();
            //If game isn't over in 1st betting phase
            if(!isGameOver) {
                //Discard cards
                discardPhase();
                //Second betting phase
                bettingPhase();
                //Finds winners
                winners = comparisonPhase();
                isGameOver = true;
            }
            //Finds winner
            winners = comparisonPhase();
        }
        System.out.println("The winner(s) is/are:");
        givePotToWinners(winners, findWinner(winners));
        //Reset phase
        discardRemainingCards();
        returnCardsToDeck();
        resetFolds();
    }
}